# Descomplicando o GitLab3

Treinamento GitLab na Twitch com o Jeferson Noronha

##### Day-1 #####

- Entendemos o que é o Git
- Entendemos o que é o GitLab
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git
- Como criar uma branch
- Como criar um merge requesst
- Como criar um repositorio git
- Como adicionar um Membro no projeto
- Como fazer o merge na Master/Main
